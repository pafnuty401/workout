$(function(){
  let mode = [
    {title : "EASY", color: "#4CAF50"},
    {title : "MODERATE", color: "#FFCC00"},
    {title : "HARD", color: "#FF6600"},
    {title : "ALL-OUT", color: "FF0000"},
  ];

  let timetable = [ //time: mm:ss
    {time : "05:00", "mode" : 0},
    {time : "00:30", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "00:30", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "00:30", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "00:30", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "01:00", "mode" : 0},
    {time : "01:00", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "01:00", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "01:00", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "01:00", "mode" : 2},
    {time : "00:30", "mode" : 1},
    {time : "01:00", "mode" : 0},
    {time : "00:45", "mode" : 3},
    {time : "00:15", "mode" : 0},
    {time : "00:45", "mode" : 3},
    {time : "00:15", "mode" : 0},
    {time : "00:45", "mode" : 3},
    {time : "00:15", "mode" : 0},
    {time : "02:00", "mode" : 0}, //possible to repeat the entire circuit starting from here
    {time : "05:00", "mode" : 0},
 ];

  interval = 0;
  reset();
  $("#start").click(go);
  $("#stop").click(reset);


  function printTimetable() {
    let html = '<table class="table">';
    timetable.forEach(function(item, i , arr) {
      let color = mode[item.mode].color;
      html += '<tr id="tr' + i + '">';
      html += '<td bgcolor="' + color + '">' + mode[item.mode].title + '</td><td bgcolor="' + color + '">' + item.time + '</td>';
      html += '</tr>';
    });
    html += '</table>';
    $('.timetable').html(html);
  }

  function reset() {
    let line = getLine(0);
    setIntensity(line.title, line.color);
    setTime(line.time);
    $(".timer").show();
    switchButtons();
    if (interval) {
      clearInterval(interval);
    }
    printTimetable();
  }

  function getTime(timeString) {
    let data = timeString.split(":");
    return Number(data[0]) * 60 + Number(data[1]);
  }

  function getLine(index) {
    let timetableLine = timetable[index];
    let modeLine = mode[timetableLine.mode];
    return {time: getTime(timetableLine.time), title: modeLine.title, color: modeLine.color}
  }

  function setTime(time) {
    var date = new Date(null);
    date.setSeconds(time);
    $("#time").html(date.toISOString().substr(14, 5));
  }

  function setIntensity(title, color) {
    $("#intensityTitle").html(title);
    $(".intensity").css("background-color", color);
  }

  function switchButtons() {
      $("#start").toggle();
      $("#stop").toggle();
  }

  function getTotalTime() {
    let totalTime = 0;
    timetable.forEach(function(item, i, arr) {
      totalTime += getTime(item.time);
    });

    return totalTime;
  }

  function go() {
    switchButtons();
    let totalTime = getTotalTime();
    i = -1;
    let currentTime = -1;
    interval = setInterval(function () {
      if (currentTime < 0) {
        if (i >= 0) {
          $('tr#tr' + i).fadeOut();
        }
        i++;
        let line = getLine(i);
        $('tr#tr' + i).css('border','15px solid #42AAF4');
        currentTime = line.time;
        let currentTitle = line.title;
        let currentColor = line.color;
        setIntensity(line.title, line.color);
      }
      setTime(currentTime);
      currentTime--;
    }, 1000);

     setTimeout(function () {
       reset();
     }, totalTime * 1000);

  }
})
